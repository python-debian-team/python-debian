#!/usr/bin/make -f
# -*- makefile -*-

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

include /usr/share/dpkg/pkg-info.mk

export SETUPTOOLS_SCM_PRETEND_VERSION = $(shell debian/make_pep440_compliant "$(DEB_VERSION)")

# tell pybuild which module to install into the packages
export PYBUILD_NAME=debian

# for packaging, insist that all optional dependencies are installed
# when running the test suite
export FORBID_MISSING_APT_PKG ?= 1
export FORBID_MISSING_CHARSET_NORMALIZER ?= 1
export FORBID_MISSING_GPGV ?= 1
export FORBID_MISSING_AR ?= 1
export FORBID_MISSING_DPKG_DEB ?= 1
export FORBID_MISSING_ZSTD ?= 1

%:
	dh $@ --with python3 --buildsystem pybuild

execute_after_dh_auto_build:
	src/debian/doc-debtags > README.debtags

# don't run the tests if suppressed with DEB_BUILD_OPTIONS=nocheck
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
override_dh_auto_test: test
endif

dist:
	# needs python3-build python3-wheel
	rm -f dist/python?debian-$(SETUPTOOLS_SCM_PRETEND_VERSION)*
	python3 -m build --skip-dependency-check --no-isolation --sdist --wheel
	@echo "You can now:"
	@echo "  twine check --strict dist/python?debian-$(SETUPTOOLS_SCM_PRETEND_VERSION)*"
	@echo "  twine upload dist/python?debian-$(SETUPTOOLS_SCM_PRETEND_VERSION)*"

apidoc:
	rm -f docs/api/*
	cd src && sphinx-apidoc -e --private -H python-debian \
	  -o ../docs/api/ . \
	  debian/tests/

doc: apidoc
	PYTHONPATH=$(CURDIR)/src \
	  make -C docs/ SPHINXOPTS="-a -v -n" html

test:
	# run all the tests with both a UTF-8 aware locale and a non-UTF-8
	# aware locale to catch errors related to encoding.
	for py in `py3versions -s`; do \
	  set -e; \
	  echo "----------------------------------------------"; \
	  echo "Testing with $$py"; \
	  echo "----------------------------------------------"; \
	  LC_ALL=C $$py -m pytest; \
	  LC_ALL=C.UTF-8 $$py -m pytest; \
	done

qa:
	@echo "Running both pylint and mypy"
	@echo "----------- pylint -----------"
	@pylint $$(find src/debian -name \*py | grep -v test | sort); \
	  pylinterr=$$?; \
	  echo "----------- mypy -----------"; \
	  mypy; \
	  mypyerr=$$? ; \
	  [ $$pylinterr -eq 0 -a $$mypyerr -eq 0 ]

.PHONY: dist doc apidoc qa
